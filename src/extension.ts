// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { DataProvider, DataItem } from './dataProvider';
// import { TreeDataProvider } from "./tree"
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "jcleng-code-show" is now active!');
	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('jcleng-code-show.CreateMDThisProject', () => {
		var os = require('os');
		var platform = os.platform(); // 'darwin', 'freebsd', 'linux', 'sunos' , 'win32'
		// * 创建文件夹
		let workspaceRootPath = vscode.workspace.rootPath;
		if (workspaceRootPath == undefined) {
			return vscode.window.showInformationMessage('请先打开工作区');
		}
		var fs = require("fs");
		let configPath = workspaceRootPath + '\\.vscode';
		if (platform == 'linux' || platform == 'darwin') {
			configPath = workspaceRootPath + '/.vscode';
		}
		fs.exists(workspaceRootPath, function (exists: boolean) {
			if (exists == false) {
				fs.mkdir(configPath, function (error: boolean) {
					if (error) {
						vscode.window.showInformationMessage('创建目录失败');
						return false;
					}
					vscode.window.showInformationMessage('创建目录成功');
				});
			}

			// * 创建文件
			var file_path = configPath + '\\' + vscode.workspace.name + '.do.md';
			if (platform == 'linux' || platform == 'darwin') {
				file_path = configPath + '/' + vscode.workspace.name + '.do.md';
			}
			fs.exists(file_path, function (exists: boolean) {
				if (exists == false) {
					fs.writeFile(file_path, '', 'utf8', function (error: boolean) {
						if (error) {
							vscode.window.showInformationMessage('创建文件失败');
							return false;
						}
						vscode.window.showInformationMessage('创建文件成功 ' + file_path);
					})
				}
				// * 打开该文件
				vscode.workspace.openTextDocument(file_path)
					.then(doc => {
						vscode.window.showTextDocument(doc);
						vscode.window.showInformationMessage(`打开文件 ${file_path}`);
					}, err => {
						vscode.window.showInformationMessage(`Open ${file_path} error`);
					}).then(undefined, err => {
						vscode.window.showInformationMessage(`Open ${file_path} error`);
					})
			});

		})

	});

	let disposable2 = vscode.commands.registerCommand('jcleng-code-show.OpenGlobalMDFile', () => {
		let config = vscode.workspace.getConfiguration().get('jcleng-code-show.globalTodoMDFile');
		if (config != undefined && config != null) {
			let global_file_path: string = String(config);
			var fs = require("fs");
			fs.exists(global_file_path, function (exists: boolean) {
				if (exists == false) {
					vscode.window.showInformationMessage('文件不存在' + global_file_path);
					return false;
				}
				// * 打开该文件
				vscode.workspace.openTextDocument(global_file_path)
					.then(doc => {
						vscode.window.showTextDocument(doc);
					}, err => {
						vscode.window.showInformationMessage(`Open ${global_file_path} error`);
					}).then(undefined, err => {
						vscode.window.showInformationMessage(`Open ${global_file_path} error`);
					});
				vscode.window.showInformationMessage(`打开文件 ${global_file_path}`);
				return;
			});
		} else {
			vscode.window.showInformationMessage('文件未配置');
			return false;
		}


	})
	// * 注册数据
	var _data = [
		new DataItem('TODO: yes1'),
		new DataItem('TODO: yes2'),
		new DataItem('TODO: yes3'),
	];
	vscode.window.createTreeView('jclengCodeShowViewsGlobal', { treeDataProvider: new DataProvider(_data) });
	context.subscriptions.push(...[disposable, disposable2]);

}

// this method is called when your extension is deactivated
export function deactivate() { }
